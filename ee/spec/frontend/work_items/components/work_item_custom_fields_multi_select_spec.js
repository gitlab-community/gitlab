import Vue, { nextTick } from 'vue';
import VueApollo from 'vue-apollo';
import { shallowMount } from '@vue/test-utils';
import * as Sentry from '~/sentry/sentry_browser_wrapper';
import waitForPromises from 'helpers/wait_for_promises';
import createMockApollo from 'helpers/mock_apollo_helper';
import WorkItemSidebarDropdownWidget from '~/work_items/components/shared/work_item_sidebar_dropdown_widget.vue';
import WorkItemCustomFieldsMultiSelect from 'ee/work_items/components/work_item_custom_fields_multi_select.vue';
import { CUSTOM_FIELDS_TYPE_MULTI_SELECT, CUSTOM_FIELDS_TYPE_NUMBER } from '~/work_items/constants';
import updateWorkItemCustomFieldsMutation from 'ee/work_items/graphql/update_work_item_custom_fields.mutation.graphql';
import customFieldSelectOptionsQuery from 'ee/work_items/graphql/work_item_custom_field_select_options.query.graphql';
import { customFieldsWidgetResponseFactory } from 'jest/work_items/mock_data';

describe('WorkItemCustomFieldsMultiSelect', () => {
  let wrapper;

  Vue.use(VueApollo);

  const defaultWorkItemType = 'Issue';
  const defaultWorkItemId = 'gid://gitlab/WorkItem/1';

  const defaultField = {
    customField: {
      id: '1-select',
      fieldType: CUSTOM_FIELDS_TYPE_MULTI_SELECT,
      name: 'Multi select custom field label',
    },
    selectedOptions: [
      {
        id: 'select-1',
        value: 'Option 1',
      },
      {
        id: 'select-2',
        value: 'Option 2',
      },
    ],
  };

  const querySuccessHandler = jest.fn().mockResolvedValue({
    data: {
      group: {
        id: 'gid://gitlab/Group/33',
        customField: {
          id: '1-select',
          selectOptions: [
            {
              id: 'select-1',
              value: 'Option 1',
              __typename: 'CustomFieldSelectOption',
            },
            {
              id: 'select-2',
              value: 'Option 2',
              __typename: 'CustomFieldSelectOption',
            },
            {
              id: 'select-3',
              value: 'Option 3',
              __typename: 'CustomFieldSelectOption',
            },
          ],
          __typename: 'CustomField',
        },
        __typename: 'Group',
      },
    },
  });

  const mutationSuccessHandler = jest.fn().mockResolvedValue({
    data: {
      workItemUpdate: {
        workItem: {
          id: defaultWorkItemId,
          widgets: [customFieldsWidgetResponseFactory],
        },
        errors: [],
      },
    },
  });

  const findComponent = () => wrapper.findComponent(WorkItemCustomFieldsMultiSelect);
  const findSidebarDropdownWidget = () => wrapper.findComponent(WorkItemSidebarDropdownWidget);
  const findSelectValues = () => wrapper.findAll('[data-testid="option-text"]');

  const createComponent = ({
    canUpdate = true,
    workItemType = defaultWorkItemType,
    customField = defaultField,
    workItemId = defaultWorkItemId,
    queryHandler = querySuccessHandler,
    mutationHandler = mutationSuccessHandler,
  } = {}) => {
    wrapper = shallowMount(WorkItemCustomFieldsMultiSelect, {
      apolloProvider: createMockApollo([
        [customFieldSelectOptionsQuery, queryHandler],
        [updateWorkItemCustomFieldsMutation, mutationHandler],
      ]),
      propsData: {
        canUpdate,
        customField,
        workItemType,
        workItemId,
        fullPath: 'flightjs/FlightJs',
      },
    });
  };

  describe('rendering', () => {
    it('renders if custom field exists and type is correct', async () => {
      createComponent();

      await nextTick();

      expect(findComponent().exists()).toBe(true);
      expect(findSidebarDropdownWidget().exists()).toBe(true);
    });

    it('does not render if custom field is empty', async () => {
      createComponent({ customField: {} });

      await nextTick();

      expect(findComponent().exists()).toBe(true);
      expect(findSidebarDropdownWidget().exists()).toBe(false);
    });

    it('does not render if custom field type is incorrect', async () => {
      createComponent({
        customField: {
          customField: {
            id: '1-number',
            fieldType: CUSTOM_FIELDS_TYPE_NUMBER,
            name: 'Number custom field label',
          },
          selectedOptions: {
            id: 'number',
            value: 3,
          },
        },
      });

      await nextTick();

      expect(findComponent().exists()).toBe(true);
      expect(findSidebarDropdownWidget().exists()).toBe(false);
    });
  });

  it('displays correct label', () => {
    createComponent();

    expect(findSidebarDropdownWidget().props('dropdownLabel')).toBe(
      'Multi select custom field label',
    );
  });

  describe('value', () => {
    it('shows None when no option is set', () => {
      createComponent({
        customField: {
          customField: {
            id: '1-select',
            fieldType: CUSTOM_FIELDS_TYPE_MULTI_SELECT,
            name: 'Multi select custom field label',
          },
          selectedOptions: null,
        },
      });

      expect(findSidebarDropdownWidget().props().toggleDropdownText).toContain('None');
    });

    it('shows None when invalid value is received', () => {
      createComponent({
        customField: {
          customField: {
            id: '1-select',
            fieldType: CUSTOM_FIELDS_TYPE_MULTI_SELECT,
            name: 'Multi select custom field label',
          },
          selectedOptions: 'Sample text',
        },
      });

      expect(findSidebarDropdownWidget().props().toggleDropdownText).toContain('None');
    });

    it('shows option selected on render when it is defined', () => {
      createComponent();

      expect(findSelectValues().at(0).props('text')).toBe('Option 1');
      expect(findSelectValues().at(1).props('text')).toBe('Option 2');
    });
  });

  describe('Dropdown options', () => {
    it('fetches options when dropdown is shown', async () => {
      createComponent();

      expect(querySuccessHandler).not.toHaveBeenCalled();

      await findSidebarDropdownWidget().vm.$emit('dropdownShown');

      expect(querySuccessHandler).toHaveBeenCalled();
    });

    it('shows dropdown options on list according to their state', async () => {
      createComponent();

      findSidebarDropdownWidget().vm.$emit('dropdownShown');
      await waitForPromises();

      expect(findSidebarDropdownWidget().props('toggleDropdownText')).toBe('Option 1, Option 2');
      expect(findSidebarDropdownWidget().props('listItems')).toEqual([
        {
          options: [
            { text: 'Option 1', value: 'select-1' },
            { text: 'Option 2', value: 'select-2' },
          ],
          text: 'Selected',
        },
        {
          options: [{ text: 'Option 3', value: 'select-3' }],
          text: 'All',
          textSrOnly: true,
        },
      ]);
    });

    it('shows "None" on value when dropdown is open and no option was selected', async () => {
      createComponent({
        customField: {
          customField: {
            id: '1-select',
            fieldType: CUSTOM_FIELDS_TYPE_MULTI_SELECT,
            name: 'Multi select custom field label',
          },
          selectedOptions: null,
        },
      });

      findSidebarDropdownWidget().vm.$emit('dropdownShown');
      await waitForPromises();

      expect(findSidebarDropdownWidget().props('toggleDropdownText')).toBe('None');
      expect(findSidebarDropdownWidget().props('listItems')).toEqual([
        { text: 'Option 1', value: 'select-1' },
        { text: 'Option 2', value: 'select-2' },
        { text: 'Option 3', value: 'select-3' },
      ]);
    });

    it('shows alert error when fetching options fails', async () => {
      const errorQueryHandler = jest.fn().mockRejectedValue(new Error('Network error'));

      createComponent({ queryHandler: errorQueryHandler });

      findSidebarDropdownWidget().vm.$emit('dropdownShown');
      await waitForPromises();

      expect(wrapper.emitted('error')).toEqual([
        [
          'Options could not be loaded for field: Multi select custom field label. Please try again.',
        ],
      ]);
    });
  });

  describe('updating the selection', () => {
    it('sends mutation with correct variables when selecting an option', async () => {
      createComponent();
      await nextTick();

      const newSelectedIds = ['select-1', 'select-2'];
      findSidebarDropdownWidget().vm.$emit('updateValue', newSelectedIds);

      expect(mutationSuccessHandler).toHaveBeenCalledWith({
        input: {
          id: defaultWorkItemId,
          customFieldsWidget: {
            customFieldId: defaultField.customField.id,
            selectedOptionIds: newSelectedIds,
          },
        },
      });
    });

    it('sends null when clearing the selection', async () => {
      createComponent();
      await nextTick();

      findSidebarDropdownWidget().vm.$emit('updateValue', []);

      expect(mutationSuccessHandler).toHaveBeenCalledWith({
        input: {
          id: defaultWorkItemId,
          customFieldsWidget: {
            customFieldId: defaultField.customField.id,
            selectedOptionIds: [],
          },
        },
      });
    });

    it('shows loading state while updating', async () => {
      const mutationHandler = jest.fn().mockImplementation(() => new Promise(() => {}));

      createComponent({ mutationHandler });
      await nextTick();

      findSidebarDropdownWidget().vm.$emit('updateValue', ['select-1', 'select-2']);
      await nextTick();

      expect(findSidebarDropdownWidget().props('updateInProgress')).toBe(true);
    });

    it('emits error event when mutation returns an error', async () => {
      jest.spyOn(Sentry, 'captureException');

      const errorMessage = 'Failed to update';
      const mutationHandler = jest.fn().mockResolvedValue({
        data: {
          workItemUpdate: {
            errors: [errorMessage],
          },
        },
      });

      createComponent({ mutationHandler });
      await nextTick();

      findSidebarDropdownWidget().vm.$emit('updateValue', ['select-1', 'select-2']);
      await waitForPromises();

      expect(wrapper.emitted('error')).toEqual([
        ['Something went wrong while updating the issue. Please try again.'],
      ]);
      expect(Sentry.captureException).toHaveBeenCalled();
    });

    it('emits error event when mutation catches error', async () => {
      jest.spyOn(Sentry, 'captureException');

      const errorHandler = jest.fn().mockRejectedValue(new Error());

      createComponent({ mutationHandler: errorHandler });
      await nextTick();

      findSidebarDropdownWidget().vm.$emit('updateValue', ['select-1', 'select-2']);
      await waitForPromises();

      expect(wrapper.emitted('error')).toEqual([
        ['Something went wrong while updating the issue. Please try again.'],
      ]);
      expect(Sentry.captureException).toHaveBeenCalled();
    });
  });
});
