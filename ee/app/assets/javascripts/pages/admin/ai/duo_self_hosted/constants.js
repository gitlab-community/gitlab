/* eslint-disable @gitlab/require-i18n-strings */

export const SELF_HOSTED_DUO_TABS = {
  SELF_HOSTED_MODELS: 'self-hosted-models',
  AI_FEATURE_SETTINGS: 'ai-feature-settings',
};

export const RELEASE_STATES = {
  GA: 'GA',
  BETA: 'BETA',
};

export const DUO_MAIN_FEATURES = {
  CODE_SUGGESTIONS: 'Code Suggestions',
  DUO_CHAT: 'GitLab Duo Chat',
};
